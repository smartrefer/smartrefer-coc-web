import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ThaiaddressService {
  accessToken: any;
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    this.accessToken = sessionStorage.getItem('accessToken');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.accessToken
      })
    };
  }

  async select(chwpart:any,amppart:any,tmbpart:any) {
    const _url = `${this.apiUrl}/address/select/${chwpart}/${amppart}/${tmbpart}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async select_full(chwpart:any,amppart:any,tmbpart:any,moopart:any) {
    const _url = `${this.apiUrl}/address/select_full/${chwpart}/${amppart}/${tmbpart}/${moopart}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }   

  async select_hospcode(hospcode:any) {
    const _url = `${this.apiUrl}/hospcode/select/${hospcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async select_all() {
    const _url = `${this.apiUrl}/hospcode/select_all`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async select_same_district(hospcode:any) {
    const _url = `${this.apiUrl}/hospcode/select_same_district/${hospcode}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async getIcd10() {
    const _url = `${this.apiUrl}/address/getIcd10`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async getProvince() {
    const _url = `${this.apiUrl}/address/getProvince`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async getDistrict(province:any) {
    const _url = `${this.apiUrl}/address/getDistrict/${province}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async getSubdistrict(province:any,district:any) {
    const _url = `${this.apiUrl}/address/getSubDistrict/${province}/${district}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

  async getVillage(province:any,district:any,subdistrict:any) {
    const _url = `${this.apiUrl}/address/getVillage/${province}/${district}/${subdistrict}`;
    return this.httpClient.get(_url,this.httpOptions).toPromise();
  }

}
