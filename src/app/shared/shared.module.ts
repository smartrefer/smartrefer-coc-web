import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatLuxonDateModule } from '@angular/material-luxon-adapter';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { FuseHighlightModule } from '@fuse/components/highlight';
import { FuseCardModule } from '@fuse/components/card';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSidenavModule } from '@angular/material/sidenav';


import { ThaiDatePipe } from '../pipes/to-thai-date-pipe';
import { ThaiAgePipe } from '../pipes/to-thai-age.pipe';
import { ThaiCidPipe } from '../pipes/to-thai-cid.pipe';
import { CatagotyNamePipe } from '../pipes/get-catagory.pipe';
import { ItemNamePipe } from 'app/pipes/item-name.pipe';
import { DialogAlert } from './alert/alert.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxSliderModule } from '@angular-slider/ngx-slider'
import { EvaluateScorePipe } from 'app/pipes/evaluate-score.pipe';




@NgModule({

    declarations: [ThaiDatePipe, ThaiAgePipe, ThaiCidPipe, DialogAlert, CatagotyNamePipe, ItemNamePipe, EvaluateScorePipe],

    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatLuxonDateModule,
        MatMenuModule,
        MatSelectModule,
        FuseHighlightModule,
        FuseCardModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatRadioModule,
        MatCheckboxModule,
        MatTabsModule,
        MatMomentDateModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatDialogModule, MatSidenavModule,
        NgxSpinnerModule,
        NgxSliderModule
    ],

    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatChipsModule,
        MatDatepickerModule,
        MatDividerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatLuxonDateModule,
        MatMenuModule,
        MatSelectModule,
        FuseHighlightModule,
        FuseCardModule,
        MatCardModule,
        MatTableModule,
        MatExpansionModule,
        MatRadioModule,
        MatCheckboxModule,
        MatTabsModule,
        MatMomentDateModule,
        MatNativeDateModule,
        MatTooltipModule,
        MatDialogModule, MatSidenavModule,
        ThaiDatePipe,
        ThaiAgePipe,
        ThaiCidPipe,
        CatagotyNamePipe,
        ItemNamePipe,
        EvaluateScorePipe,
        NgxSpinnerModule,
        NgxSliderModule
    ]
})
export class SharedModule {
}
