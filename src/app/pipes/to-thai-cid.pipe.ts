import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
    name: 'thaicid'
})
export class ThaiCidPipe implements PipeTransform {
    transform(cid: string): string {
        //   console.log(cid);
        let myArray = cid.split("");
        let tCid = myArray[0]+"-"+myArray[1]+myArray[2]+myArray[3]+myArray[4]+"-"+myArray[5]+myArray[6]+myArray[7]+myArray[8]+myArray[9]+"-"+myArray[10]+myArray[11]+"-"+myArray[12];
     
        return tCid;
    }
}