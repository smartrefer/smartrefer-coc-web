import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { Router, NavigationExtras } from '@angular/router';
import { RegisterService } from '../../../services/register.service';
import { HomevisitService } from '../../../services/homevisit.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SweetAlertService } from '../../../shared/sweetalert.service';
import { BarthelDialog } from './barthel/barthel.component';
import { AdlDialog } from './adl/adl.component';
import { EvaluateNtritionDialog } from './evaluate-nutrition/evaluate-nutrition.component';
import { ChildDevelopDialog } from './child-develop/child-develop.component';
import { ChildVisitDialog } from './child-visit/child-visit.component';
import { CvdRiskDialog } from './cvd-risk/cvd-risk.component';
import { ElderlyDialog } from './elderly/elderly.component';
import { EpidemDialog } from './epidem/epidem.component';
import { EsasDialog } from './esas/esas.component';
import { ModifiedRankingDialog } from './modified-ranking/modified-ranking.component';
import { MotorPowerDialog } from './motor-power/motor-power.component';
import { PainKillerDialog } from './pain-killer/pain-killer.component';
import { PostnatalDialog } from './postnatal/postnatal.component';
import { PpsDialog } from './pps/pps.component';
import { PsycheDialog } from './psyche/psyche.component';
import { ScreenNutritionDialog } from './screen-nutrition/screen-nutrition.component';
import { ViolentlyDialog } from './violently/violently.component';
import { AdvanceCarePlanDialog } from './advance-care-plan/advance-care-plan.component';
import { OtherDialog } from './other/other.component';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'landing-home',
  templateUrl: './sendhomevisit_view.component.html',
  styleUrls: ['./sendhomevisit_view.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SendhomevisitViewComponent {
  @ViewChild(MatAccordion) accordion: MatAccordion;
  itemStorage: any = [];
  itemReferLevel: any = [];
  itemPatientStatus: any = [];
  itemCatagory: any = [];
  itemItems: any = [];
  refer_level: any;
  refer_level_name: any;
  patient_status: any;
  patient_status_name: any;
  patient_address_name: any;
  phone_number: any;
  validateForm: boolean = false;
  panelOpenState = false;
  extra_detail: any;
  diag_text: any;
  profile: any;
  question: any;
  result: any = [];
  temp_data: any = [];
  diagnosis:any = [];
  itemName: any;
  catagoryName: any;

  info : {coc_register_id: number};

  displayedColumnsDrug: string[] = ['action', 'drugdate', 'drugname', 'druguse', 'drugqty'];
  dataSourceDrug: any = [];

  displayedColumnsLab: string[] = ['action', 'labdate', 'labname', 'labresult', 'labnormal'];
  dataSourceLab: any = [];

  displayedColumnsXray: string[] = ['action', 'xraydate', 'xrayname', 'xrayresult', 'comments'];
  dataSourceXray: any = [];

  displayedColumnsDrugAllergy: string[] = ['action', 'drugallergydate', 'drugallergyname', 'drugallergysymptom', 'drugallergylevel', 'drugallergyfind'];
  dataSourceDrugAllergy: any = [];
  step = 0;

  route: any;
  is_editable = false;
  complication: any = '';

  /**
   * Constructor
   */
  constructor(
    private router: Router,
    private registerService: RegisterService,
    private homevisitService: HomevisitService,
    private spinner: NgxSpinnerService,
    public dialog: MatDialog,
  ) {
    let username = sessionStorage.getItem('username');
    if(!username){
        this.router.navigate(['/sign-in']);
    }
    this.route = sessionStorage.getItem('route');
    this.is_editable = this.route == '/coc' ? true : false;
    let i: any = sessionStorage.getItem('itemStorage');
    this.itemStorage = JSON.parse(i);
    this.refer_level = this.itemStorage.refer_level;
    this.patient_status = this.itemStorage.patient_status;
    this.extra_detail = this.itemStorage.extra_detail;
    this.diag_text = this.itemStorage.diag_text;
    this.info = {
      coc_register_id: this.itemStorage.coc_register_id
    }
  }

  async ngOnInit() {
    this.spinner.show();
    // console.log(this.itemStorage);
    await this.list_refer_level();
    await this.list_patient_status();
    await this.list_catagory();
    await this.list_items();
    await this.select_regisOne();
    await this.getHomevisit();

    // this.getResult();
    
    this.spinner.hide();
  }

  setStep(index: number) {
    this.step = index;
  }

  async select_regisOne() {

    let rs: any = await this.registerService.select_regisOne(this.info);

    if(rs.length > 0){
      this.diagnosis = rs[0].diag;
      this.dataSourceDrug = rs[0].med;
      this.dataSourceLab = rs[0].lab;
      this.dataSourceXray = rs[0].xray;
      this.dataSourceDrugAllergy = rs[0].allergy;
      this.patient_address_name = rs[0].patient_address_name;
      this.phone_number = rs[0].phone_number;
      
    }else{
      // console.log('เกิดข้อผิดพลาด ไม่สามารถดึงข้อมูลได้');
      rs = [{
        med : [],
        lab : [],
        xray : [],
        allergy : [],
        diag : [],
        patient_address_name: '',
        phone_number: ''
      }];
    }
  }

  async list_refer_level() {
    let rs: any = await this.registerService.list_refer_level();
    if(rs.length > 0) {
      let referLevel = rs.find(x => x.refer_level == this.refer_level);
      this.refer_level_name = referLevel.refer_level_name;
    } else {
      this.refer_level_name = '';
    }
  }

  async list_patient_status() {
    let rs: any = await this.registerService.list_patient_status();
    if(rs.length > 0){
      let patientStatus = rs.find(x => x.patient_status == this.patient_status);
      this.patient_status_name = patientStatus.patient_status_name;
    } else {
      this.patient_status_name = '';
    }

  }

  async list_catagory() {
    let rs: any = await this.registerService.list_catagory();
    this.itemCatagory = rs;
    const catagory_list = this.itemStorage.evaluate_cause;
    if(catagory_list == null || catagory_list == '' || catagory_list == undefined) {
      this.catagoryName = '';
      return;
    }
    let catagory = catagory_list.split(',');
    let catagory_name: any = '';
    if(catagory.length >0) {
      catagory.forEach(element => {
        let rs: any = this.itemCatagory.find(x => x.catagory_id == element);
        if(rs.catagory_name != undefined){
          catagory_name += rs.catagory_name + ',';
        } 
      });
      this.catagoryName = catagory_name.substring(0, catagory_name.length - 1);
    }
  }

  async list_items() {
    let rs: any = await this.registerService.list_items();
    this.itemItems = rs;
    const item_list = this.itemStorage.items;
    if(item_list == null || item_list == '' || item_list == undefined) {
      this.itemName = '';
      return;
    }
    let item = item_list.split(',');
    let item_name = '';
    if(item.length >0) {
      item.forEach(element => {
        let rs : any = this.itemItems.find(x => x.id == element);
        if(rs.name != undefined) {
          item_name += rs.name + ',';
        }
      });
      this.itemName = item_name.substring(0, item_name.length - 1);
    }
  }

  async getHomevisit() {
    try {
      // รับข้อมูล ลงเยี่ยมบ้าน
      let rs: any = await this.homevisitService.getHomeVisit(this.info);
      // console.log('xx' + rs);
      this.profile = rs;

      // ภาวะแทรกซ้อน
      let _complication = JSON.parse(this.profile[0].complication);
      let d1 = (_complication[0].pressure_sores_size) ? _complication[0].pressure_sores_size : ' - ';
      let d2 = (_complication[0].pressure_sores_location) ? _complication[0].pressure_sores_location : ' - ';
      let d3 = (_complication[0].stiffness_location) ? _complication[0].stiffness_location : ' - ';
      let d4 = (_complication[0].other_text) ? _complication[0].other_text : ' - ';
      let _pressure_sores = (_complication[0].pressure_sores == true) ? 'แผลกดทับ' + ' ขนาด ' + d1 + ' ตำแหน่ง ' + d2 + ', ' : '';
      let _lung_infection = (_complication[0].lung_infection == true) ? 'ปอดติดเชื้อ, ' : '' ;
      let _stiffness = (_complication[0].stiffness == true) ? 'ข้อยืดติด' + ' ตำแหน่ง ' + d3 + ', ' : '';
      let _infection = (_complication[0].infection == true) ? 'แผลอักเสบ/ติดเชื้อ, ' : '' ;
      let _urinary_tract_infection = (_complication[0].urinary_tract_infection == true) ? 'ติดเชื้อระบบทางเดินปัสสาวะ, ' : '' ;
      let _depression = (_complication[0].depression == true) ? 'ภาวะซึมเศร้า, ' : '' ;
      let _other = (_complication[0].other == true) ? 'อื่นๆ(ระบุ) ' + d4 + ', ' : '';
      this.complication = ((_complication[0].complication == '0') ? 'ไม่มี' : '' + _pressure_sores + _lung_infection + _stiffness + _infection + _urinary_tract_infection + _depression + _other).replace(/, *$/, "");
 
      // รับข้อมูล คำถาม
      this.question = await this.homevisitService.getQuestion();
        // console.log(this.question);
        if (this.question.length > 0) {
          this.question = this.question.splice(3, 24);
        } 
      // console.log(this.question);

      // รับข้อมูล การประเมิน
      if(this.profile.length > 0 ){
        this.profile.forEach(async element => {
          element.result = await this.getResult(element.coc_home_id);
        });
      }
      // รับข้อมูล diagnosis
      // let diag = await this.homevisitService.getDiagnosis(this.info);
      // this.diagnosis = diag;

      // console.log(diag);
      // console.log(this.profile);

    } catch (error) {
      console.log(error);
    }
    // console.log('get done')
  }

  async getEvaluate(id: any, question_type_id: any) {

    let info: any = {
      "coc_home_id": id,
      "question_type_id": question_type_id
    }

    try {
      let rs: any = await this.homevisitService.getEvaluate(info);
      // console.log(rs);
      return rs;
    } catch (error) {
      console.log(error);
    }

  }

  async getEvaluateScore(id: any, question_type_id: any) {
    let info: any = {
      "coc_home_id": id,
      "question_type_id": question_type_id
    }
    try {
      let rs: any = await this.homevisitService.getEvaluateScore(info);
      // console.log(rs);
      return rs;
    } catch (error) {
      console.log(error);
    }
  }

  async getResult(id: any) {
    let result: any = [];

    this.question.forEach(async element => {
      try {
        let e: any = await this.getEvaluate(id, element.question_type_id);
        let c = await this.getEvaluateScore(id, element.question_type_id);
        //console.log(e);
        let data: any = {
          question_type_id: element.question_type_id,
          question_text: element.questtion_type_name,
          evaluate_data: e,
          evaluate_score: c[0]
        }
        if (e.length > 0) {
          result.push(data);
        }
      }
      catch (error) {
        console.log(error);
      }
    });
    // console.log(result);
    return result;
  }

  async openDialog(question: any,len:any,i:any) {
    // console.log(len);
    // console.log(i);
    let lastVisit  = (len == i) ? 'Y' :'N';
    if (question.question_type_id == 4) {
      this.openDialogPsyche(question,lastVisit);
    }
    if (question.question_type_id == 5) {
      this.openDialogViolent(question,lastVisit);
    }
    if (question.question_type_id == 6) {
      this.openDialogBarthel(question,lastVisit);
    }
    if (question.question_type_id == 7) {
      this.openDialogPps(question,lastVisit);
    }
    if (question.question_type_id == 8) {
      this.openDialogEsas(question,lastVisit);
    }
    if (question.question_type_id == 9) {
      this.openDialogPain(question,lastVisit);
    }
    if (question.question_type_id == 10) {
      this.openDialogAdl(question,lastVisit);
    }
    if (question.question_type_id == 11) {
      this.openDialogScreen(question,lastVisit);
    }
    if (question.question_type_id == 12) {
      this.openDialogEvaluateNutrition(question,lastVisit);
    }
    if (question.question_type_id == 13) {
      this.openDialogModified(question,lastVisit);
    }
    if (question.question_type_id == 14) {
      this.openDialogMotor(question,lastVisit);
    }
    if (question.question_type_id == 15) {
      this.openDialogPostnatal(question,lastVisit);
    }
    if (question.question_type_id == 16) {
      this.openDialogChildVisit(question,lastVisit);
    }
    if (question.question_type_id == 17) {
      this.openDialogChildDev(question,lastVisit);
    }
    if (question.question_type_id == 18) {
      this.openDialogEpidem(question,lastVisit);
    }
    if (question.question_type_id == 19) {
      this.openDialogElderly(question,lastVisit);
    }
    if (question.question_type_id == 20) {
      this.openDialogAdvance(question,lastVisit);
    }
    if (question.question_type_id == 21) {
      this.openDialogOther(question,lastVisit);
    }
    if (question.question_type_id == 22) {
      this.openDialogChronic(question,lastVisit);
    }
  }

  async openDialogPsyche(data,_lastVisit) {
    const dialogRef = this.dialog.open(PsycheDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogViolent(data,_lastVisit) {
    const dialogRef = this.dialog.open(ViolentlyDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogBarthel(data,_lastVisit) {
    const dialogRef = this.dialog.open(BarthelDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogPps(data,_lastVisit) {
    const dialogRef = this.dialog.open(PpsDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogEsas(data,_lastVisit) {
    const dialogRef = this.dialog.open(EsasDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogPain(data,_lastVisit) {
    const dialogRef = this.dialog.open(PainKillerDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogAdl(data,_lastVisit) {
    const dialogRef = this.dialog.open(AdlDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
      data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogScreen(data,_lastVisit) {
    const dialogRef = this.dialog.open(ScreenNutritionDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogEvaluateNutrition(data,_lastVisit) {
    const dialogRef = this.dialog.open(EvaluateNtritionDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogModified(data,_lastVisit) {
    const dialogRef = this.dialog.open(ModifiedRankingDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogMotor(data,_lastVisit) {
    const dialogRef = this.dialog.open(MotorPowerDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogPostnatal(data,_lastVisit) {
    const dialogRef = this.dialog.open(PostnatalDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogChildVisit(data,_lastVisit) {
    const dialogRef = this.dialog.open(ChildVisitDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogChildDev(data,_lastVisit) {
    const dialogRef = this.dialog.open(ChildDevelopDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogEpidem(data,_lastVisit) {
    const dialogRef = this.dialog.open(EpidemDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogElderly(data,_lastVisit) {
    const dialogRef = this.dialog.open(ElderlyDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogAdvance(data,_lastVisit) {
    const dialogRef = this.dialog.open(AdvanceCarePlanDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogOther(data,_lastVisit) {
    const dialogRef = this.dialog.open(OtherDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async openDialogChronic(data,_lastVisit) {
    const dialogRef = this.dialog.open(CvdRiskDialog, {
      autoFocus: false,
      width: '600px',
      maxHeight: '90vb',
     data: { coc_home_id: this.itemStorage.coc_home_id, evaluate: data,lastVisit:_lastVisit }
    });
    dialogRef.afterClosed().subscribe(result => {
      // console.log('The dialog was closed');
    });
  }

  async back() {

    this.router.navigate([this.route]);
  }

}
