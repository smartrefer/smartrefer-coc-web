

import { Route } from '@angular/router';
import {ExamplePageComponentComponent} from 'app/modules/landing/example-page.component/example-page.component.component'

export const examplePageRoutes : Route[] = [
    {
        path     : '',
        component: ExamplePageComponentComponent
    }
];

