import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { DialogData } from '../../home-visit/dialog-data'
import { ThaiaddressService } from '../../../../services/thaiaddress.service';
import { DateAdapter } from '@angular/material/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../../../services/register.service';
import { SweetAlertService } from '../../../../shared/sweetalert.service';


@Component({
    selector: 'dialog-transfer',
    templateUrl: './dialog-transfer.component.html',
    encapsulation: ViewEncapsulation.None
})

export class DialogTransfer {

    patient: any;
    searchHospital: string = '';
    refer_to_hcode: any;
    filteredHospital: any[] = [];
    list_all_hospital: any = [];
    refer_to_hospname: string = '';
    is_select_hospcode: boolean = false;
    hcode: string;
    list_hospital: any = [];
    answer: any = [];
    
    constructor(
        private router: Router,
        private thaiaddressService: ThaiaddressService,
        private dateAdapter: DateAdapter<Date> ,
        public dialog: MatDialog,
        private registerService: RegisterService,
        public matDialogRef: MatDialogRef<DialogTransfer>,
        private sweetAlertService: SweetAlertService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
        
    ) {
        this.dateAdapter.setLocale('th-TH');
        this.hcode = sessionStorage.getItem('hcode');
        if (!this.hcode) {
            this.hcode = localStorage.getItem('hcode');
        }

        let username = sessionStorage.getItem('username');
        if (!username) {
            this.router.navigate(['/sign-in']);
        }
        this.list_all_hospital = localStorage.getItem('list_all_hospital');
        this.list_all_hospital = JSON.parse(this.list_all_hospital);
     }

    ngOnInit(): void {

        this.patient = this.data;
        console.log(this.patient);
        this.getHospital();

    }
    
    /**
     * Save and close
     */
    async saveAndClose()
     {
        if (this.refer_to_hcode){
            try {
                let rs:any =[];
                let info: any = {
                    "coc_register_id": this.patient.data.coc_register_id,
                    "refer_to_pcu": this.refer_to_hcode
                }
                rs = await this.registerService.update(info, this.patient.data.coc_register_id);
                console.log('res:',rs);
            } catch (error) {
                console.log(error);
            }

            this.matDialogRef.close(this.data.answer);
            location.reload();
        }else{
            this.sweetAlertService.error(
                'คำชี้แจง',
                'ไม่ระบุ ส่งต่อหน่วยเยี่ยมบ้าน',
                'SmartRefer Ubon'
            );
            
        }
        
     }

     search() {
        let is_number : boolean = false;
        if (!isNaN(Number(this.searchHospital))) {
            is_number = true;
        }
        if (this.searchHospital.length == 5 && is_number) {
            this.refer_to_hcode = this.searchHospital;
            let rs: any = this.list_all_hospital.filter((hospital: any) =>
                hospital.code.includes(this.searchHospital)
            );
            if (rs.length == 1) {
                this.refer_to_hcode = rs[0].code;
                this.filteredHospital = rs;
            }
        } else if(!is_number && this.searchHospital.length > 3){
            //search from lis all hospital
            this.filteredHospital = this.list_all_hospital.filter((hospital: any) =>
                hospital.name.toLowerCase().includes(this.searchHospital.toLowerCase())
            );
            if(this.filteredHospital.length >= 1){
                this.refer_to_hcode = this.filteredHospital[0].code;
            }
            // console.log('search from lis all hospital', this.filteredHospital);
        } else {
            this.refer_to_hospname = '';
            this.filteredHospital = [];
        }
        // console.log(this.filteredHospital);
        
    }

    onSelectionChange(event: any):void{
        // console.log(event);
        this.is_select_hospcode = true;
        this.refer_to_hcode = event.option.value;
    }

    async getHospital() {
        let rs: any = await this.thaiaddressService.select_same_district(
            this.hcode
        );
        this.list_hospital = rs;
        this.filteredHospital = this.list_hospital;
    //    console.log(this.filteredHospital)
    }

    async getAllHospital() {
        let rs: any = await this.thaiaddressService.select_all();
        // console.log(rs);
        this.list_all_hospital = rs;
    }

}