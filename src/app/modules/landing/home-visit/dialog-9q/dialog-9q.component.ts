import { Component, ViewEncapsulation, Inject  } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { DialogData } from '../dialog-data';

@Component({
    selector: 'dialog-9q',
    templateUrl: './dialog-9q.component.html',
    encapsulation: ViewEncapsulation.None
})

export class Dialog9q {
    
    answer: any = [];
    question: any;

    constructor(
        public matDialogRef: MatDialogRef<Dialog9q>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }


    ngOnInit(): void {

        this.question = this.data.question;

    }

    /**
     * Save and close
     */
     saveAndClose(): void
     {

         // Save the message as a draft
        this.question.forEach(element => {
            // console.log(element);
            let result = {
                "coc_register_id": this.data.coc_register_id,
                "question_id": element.question_id,
                "answer_score": element.score,
                "question_type_id": element.question_type_id,
                "record_by": localStorage.getItem('username')
            }
            this.answer.push(result);
        });
            // console.log(this.answer);
            this.data.answer = this.answer;
            // console.log(this.data);          

        this.matDialogRef.close(this.data.answer);
     }
 
     /**
      * Discard the message
      */
     discard(): void
     {
 
     }
 
     /**
      * Save the message as a draft
      */
     saveAsDraft(): void
     {
 
     }
 
     /**
      * Send the message
      */
     send(): void
     {
 
     }
}